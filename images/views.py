from django.shortcuts import render
from django.http import JsonResponse
from .models import Product


# Create your views here.
def index(request):
    images_list = []
    for item in Product.objects.all():
        d = {'name': item.name, 'url': item.url}
        images_list.append(d)

    return JsonResponse(images_list, safe=False)
